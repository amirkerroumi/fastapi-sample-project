FROM python:3.10.2

ADD ./app /fastapi-sample-project

COPY requirements/requirements-lock.txt /requirements.txt

WORKDIR /fastapi-sample-project

RUN pip3 install pip -U \
    && pip3 install -r /requirements.txt --trusted-host lsicloud.pages.lsi

EXPOSE 8000

CMD ["uvicorn", "app.main:app", "--host=0.0.0.0", "--port=8000", "--reload"]